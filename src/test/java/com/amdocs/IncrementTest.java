package com.amdocs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author <a href="mailto:AjeetKumar.Singh1@britebill.com">Ajeet</a>
 */

public class IncrementTest {

    @Test
    public void incrementTest() {
        assertEquals("Increment",1, new Increment().getCounter());
    }

    @Test
    public void decrementTestZero() {

        assertEquals("Decrement",1, new Increment().decreasecounter(0));
    }

    @Test
    public void decrementTestOne() {

        final Increment obj = new Increment();
        obj.getCounter();
        assertEquals("Decrement",2, obj.decreasecounter(1));
    }

    @Test
    public void decrementTestNumber() {
        final Increment obj = new Increment();
        obj.getCounter();
        obj.getCounter();

        assertEquals("Decrement",1, new Increment().decreasecounter(5));
    }
}
