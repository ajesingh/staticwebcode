package com.amdocs;


import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:AjeetKumar.Singh1@britebill.com">Ajeet</a>
 */

public class CalculatorTest {

    @Test
    public void addTest() {
        assertEquals("add",9, new Calculator().add());
    }

    @Test
    public void subTest() {
        assertEquals("sub",3, new Calculator().sub());
    }
}
